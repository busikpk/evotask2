from django.db import models

RANDOM_NAMES_COUNT = 3


class Name(models.Model):
    name = models.CharField(max_length=256, unique=True)

    @staticmethod
    def get_random_names():
        return Name.objects.all().order_by('?')[:RANDOM_NAMES_COUNT]