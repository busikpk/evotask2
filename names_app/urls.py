from django.conf.urls import url

from .views import *


urlpatterns = [
    url(r'^$', index),
    url(r'^name$', NameView.as_view()),
    url(r'^name/random$', get_random_names)
]