from django.shortcuts import render
from django.http import JsonResponse
from django.views.generic import View
from django.db.utils import IntegrityError

from json import loads, JSONDecodeError
import logging

from .models import Name

logger = logging.getLogger(__name__)


def index(request):
    return render(request, 'index.html')


def get_random_names(request):
    names = Name.get_random_names()
    return JsonResponse(_response('ok', data=[n.name for n in names]))


class NameView(View):
    def post(self, request):
        try:
            name_value = _parse_request(request)
        except ValueError as err:
            logger.error(err)
            return JsonResponse(_response('error', str(err)))

        if name_value:
            try:
                name_obj = Name(name=name_value)
                name_obj.save()
                return JsonResponse(_response('ok', 'name "{}" was saved!'.format(name_value)))
            except IntegrityError as err:
                logger.error(err)
                return JsonResponse(_response('error', '"{}" already exist'.format(name_value)))

        return JsonResponse(_response('error', 'name value not set'))

    def delete(self, request):
        try:
            name_value = _parse_request(request)
        except ValueError as err:
            logger.error(err)
            return JsonResponse(_response('error', str(err)))

        if name_value:
            Name.objects.filter(name=name_value).delete()
            return JsonResponse(_response('ok', 'name "{}" was deleted!'.format(name_value)))

        return JsonResponse(_response('error', 'name value not set'))


def _parse_request(request):
    try:
        name_value = loads(request.body.decode('utf-8')).get('name', None)
        return name_value
    except UnicodeDecodeError as err:
        logger.error(err)
        raise ValueError('can\'t convert to unicode')
    except JSONDecodeError as err:
        logger.error(err)
        raise ValueError('can\'t parse json')


def _response(status, msg=None, data=None):
    return {'status': status, 'message': msg, 'data': data}
