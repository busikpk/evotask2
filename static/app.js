const RANDOM_NAMES_BLOCK = document.getElementById('randomNamesBlock');
const NAME_INPUT = document.getElementById('nameField');
const MSG_FIELD = document.getElementById('msg');

document.getElementById('addBtn').addEventListener('click', e => nameAction('post'));

document.getElementById('removeBtn').addEventListener('click', e => nameAction('delete'));

document.getElementById('randomBtn').addEventListener('click', e => {
    showMsg('Loading!...');
    RANDOM_NAMES_BLOCK.innerHTML = '';
    fetch('name/random').
        then(res => res.json()).
        then(json => {
            showMsg('');
            drawNames(json.data)
        }).
        catch(err => showMsg(err));
});

NAME_INPUT.addEventListener('click', e => {
    MSG_FIELD.innerHTML = '';
});

function showMsg(msg) {
    MSG_FIELD.innerHTML = msg;
}

function drawNames(names) {
    const fragment = document.createDocumentFragment();
    for (const name of names) {
        const li = document.createElement('li');
        const text = document.createTextNode(name);
        li.appendChild(text);
        fragment.appendChild(li);
    }
    RANDOM_NAMES_BLOCK.innerHTML = '';
    RANDOM_NAMES_BLOCK.appendChild(fragment);
}

function nameAction(method) {
    if (NAME_INPUT.value) {
        fetch('/name', {
            method: method,
            body: JSON.stringify({name: NAME_INPUT.value})
        }).
            then(res => res.json()).
            then(json => showMsg(json.message)).
            catch(err => showMsg(err))
    } else {
        showMsg('please enter name');
    }
}